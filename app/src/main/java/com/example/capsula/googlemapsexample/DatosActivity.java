package com.example.capsula.googlemapsexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by Desarrollo3 on 27/02/2017.
 */

public class DatosActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);
        DatosFragment fragment = (DatosFragment) getSupportFragmentManager().findFragmentById(R.id.body_datos);
        if(fragment == null){
            fragment = DatosFragment.newInstance();
            Bundle bundle = new Bundle();
            //bundle.putString("email", email);
            // Envía el parámetro recogido a camara_fragment
            fragment.setArguments(bundle);
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),fragment,R.id.body_datos);
        }
    }
}
