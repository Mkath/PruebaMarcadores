package com.example.capsula.googlemapsexample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * Created by Desarrollo3 on 27/02/2017.
 */

public class DatosFragment extends BaseFragment {

    public static DatosFragment newInstance() {
        return new DatosFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_datos, container, false);
        ButterKnife.bind(this, v);
        //SOLO CREAR VISTAS O MODIFICAS VISTAS
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }
}