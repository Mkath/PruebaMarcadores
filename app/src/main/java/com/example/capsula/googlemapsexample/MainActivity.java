package com.example.capsula.googlemapsexample;


import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap map) {
        // Add a marker in Sydney, Australia, and move the camera.

        LatLng sydney = new LatLng(-12.0769479,-77.0828039);
        map.addMarker(new MarkerOptions().position(sydney).title("Plaza San Miguel").snippet("Soy un snippet"));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney,10));
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //Intent intent= new Intent(getApplicationContext(),DatosActivity.class);
                //startActivity(intent);
                DialogFragment alertDialExample = new MyDialogFragment();
                alertDialExample.show(getFragmentManager(), "AlertDialogFragment");

                return true;
            }
        });
    }

}
